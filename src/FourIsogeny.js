const BigInteger = require('jsbn').BigInteger;

const MontgomeryCurve = require('./MontgomeryCurve.js');
const F2elm = require('./F2elm.js');
const F2Point = require('./F2Point.js');

class FourIsogeny extends MontgomeryCurve {
    constructor(curve) {
        this._coeff = [];

        this._a = curve._a;
        this._b = curve._b;
        this._c = curve._c;
        this._update();
    }

    getCoeff() {
        return this._coeff;
    }

    first4Isog(p) {
        var px = p.getX();
        var pz = p.getZ();

        var t0 = F2elm.ONE;
        t0 = t0.add(t0);
        this._c = this._a.subtract(t0);

        t0 = new F2elm(new BigInteger('6'), BigInteger.ZERO);
        this._a = this._a.add(t0);
        this._a = this._a.add(this._a);
        var t1 = px.add(pz);
        var t2 = px.subtract(pz);
        t1 = t1.square();
        t2 = t2.square();
        pz = px.multiply(pz);
        pz = pz.negate();
        pz = pz.multiply(this._c);
        px = t1.subtract(pz);
        pz = t2.multiply(pz);
        px = px.multiply(t1);

        this._update();

        return new F2Point(px, pz);
    }
    get4Isog(p) {
        var px = p.getX();
        var pz = p.getZ();

        this._coeff[0] = px.add(pz);
        this._coeff[3] = px.square();
        this._coeff[4] = pz.square();
        this._coeff[0] = this._coeff[0].square();
        this._coeff[1] = this._coeff[3].add(this._coeff[4]);
        this._coeff[2] = this._coeff[3].subtract(this._coeff[4]);
        this._coeff[3] = this._coeff[3].square();
        this._coeff[4] = this._coeff[4].square();
        this._coeff[0] = this._coeff[0].subtract(this._coeff[1]);

        this._update();
    }

    eval4Isog(p) {
        var px = p.getX();
        var pz = p.getZ();

        px = px.multiply(this._coeff[0]);
        var t0 = pz.multiply(this._coeff[1]);
        px = px.subtract(t0);
        pz = pz.multiply(this._coeff[2]);
        t0 = px.subtract(pz);
        pz = pz.multiply(px);
        t0 = t0.square();
        pz = pz.add(pz);
        pz = pz.add(pz);
        px = pz.add(t0);
        pz = pz.multiply(t0);
        pz = pz.multiply(this._coeff[4]);
        t0 = t0.multiply(this._coeff[4]);
        var t1 = px.multiply(this._coeff[3]);
        t0 = t0.subtract(t1);
        px = px.multiply(t0);

        return new F2Point(px, pz);
    }
}

module.exports = FourIsogeny;