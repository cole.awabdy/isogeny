const BigInteger = require('jsbn').BigInteger;

const utils = require('./utils.js');

const REFERENCE_PRIME = new BigInteger('5');


class Felm {
    static fromBytes(bytes, prime) {
        
        var val = new BigInteger(Buffer.from(bytes).toString('hex'), 16);
        return new Felm(val, prime);
    }
    static fromFelm(felm) {
        return new Felm(felm.getValue(), felm.getPrime());
    }
    constructor(value, prime)
    {
        
        utils.checkBigIntegers(value, prime);

        this._p = prime;
        this._value = value.mod(this._p);
        
        this._psize = Math.ceil(this._p.bitLength() / 8);
        
    }
    
    getPrime() {
        return this._p;
    }
    
    getValue() {
        return this._value;
    }
    add(y) {
        return new Felm(this._value.add(y._value), this._p);
    }
    subtract(y) {
        return this.add(y.negate());
    }
    multiply(y)
    {
        return new Felm(this._value.multiply(y._value), this._p)
    }
    square() {
        return this.multiply(this);
    }
    isZero() {
        return this._value.equals(BigInteger.ZERO);
    }
    isEven() {
        var c = this._value.and(BigInteger.ONE);
    }
    isOdd() {
        return !this.isEven();
    }
    inverse() {
        return new Felm(this._value.modInverse(this._p), this._p);
    }
    equals(y) {
        return this._value.equals(y._value);
    }
    isLessThan(y) {
        return this._value.compareTo(y._value) == -1;
    }
    isGreaterThan(y) {
        return this._value.compareTo(y._value) == 1;
    }
    negate() {
        return new Felm(this._p.subtract(this._value), this._p);
    }
    div2() {
        var v = this._value;
        if(this.isOdd())
        {
            v = v.add(this._p);
            v = v.shiftRight(1);
        }
        return new Felm(v, this._p);
    }
    swap(y, option) {
        var temp, mask, yval;

        yval = y._value;
        mask = option.negate();

        temp = mask.and(this._value.xor(yval));
        this._value = temp.xor(this._value);
        yval = temp.xor(yval);

        return new Felm(yval, this._p);
    }

    toString() {
        return this._value.toString();
    }

    toByteArray() {
        var valsize = Math.ceil(this._value.bitLength() / 8);
        let paddingSize = this._psize - valsize;
        var ret = new Uint8Array(this._psize);
        ret.set(this._value.toByteArray(), paddingSize);
        return ret;
    }

}


Felm.ZERO = new Felm(BigInteger.ZERO, REFERENCE_PRIME);
Felm.ONE = new Felm(BigInteger.ONE, REFERENCE_PRIME);

module.exports = Felm;