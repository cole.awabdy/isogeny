const F2elm = require('./F2elm.js');

class F2Point {
    static fromFelms(x0, x1, z0, z1) {
        return new F2Point(new F2elm(x0, x1), new F2elm(z0, z1));
    }
    static fromPoint(p) {
        return new F2Point(p._x, p._z);
    }
    constructor(xc, zc) {
        if(xc.getPrime() != zc.getPrime()) {
            throw new Error('Cannot create F2Point with elements from different prime fields');
        }
        this._x = xc;
        this._z = zc
    }
    getX() {
        return this._x;
    }
    getZ() {
        return this._z;
    }
    swapPoints(q, option) {
        var qx = this._x.swap(q._x, option);
        var qz = this._z.swap(q._z, option);

        return new F2Point(qx, qz);
    }
    toString() {
        return this._x.multiply(this._z.inverse()).toString();
    }
}

F2Point.INFINITY = new F2Point(F2elm.ONE, F2elm.ZERO);
module.exports = F2Point;