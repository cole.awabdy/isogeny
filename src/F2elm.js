const Felm = require('./Felm.js');
const BigInteger = require('jsbn').BigInteger;
const utils = require('./utils.js');

class F2elm {
    static fromInts(a0, a1, prime) {
        utils.checkBigIntegers(a0, a1);
        return new F2elm(new Felm(a0, prime), new Felm(a1, prime));
    }
    static fromF2elm(a) {
        return new F2elm(a._x0, a._x1);
    }
    static fromBytes(bytes, prime) {
        
        var len = bytes.length / 2;
        return new F2elm(Felm.fromBytes(bytes.slice(0, len), prime), Felm.fromBytes(bytes.slice(len, len*2), prime));
    }
    constructor(a0, a1) {
        if(!(a0.getPrime().equals(a1.getPrime())))
        {
            throw new Error('Cannot create F2elm with Felms of two different prime fields');
        }

        this._x0 = a0;
        this._x1 = a1;
    }
    get0() {
        return this._x0;
    }
    get1() {
        return this._x1;
    }
    equals(y) {
        return this._x0.equals(y._x0) && this._x1.equals(y._x1);
    }
    add(y) {
        var x0 = this._x0.add(y._x0);
        var x1 = this._x1.add(y._x1);

        return new F2elm(x0, x1);
    }
    subtract(y) {
        var x0 = this._x0.subtract(y._x0);
        var x1 = this._x1.subtract(y._x1);
        return new F2elm(x0, x1);
    }
    negate() {
        return new F2elm(this._x0.negate(), this._x1.negate());
    }
    square() {
        var t1 = this._x0.add(this._x1);
        var t2 = this._x0.subtract(this._x1);
        var t3 = this._x0.add(this._x0);
        var c0 = t1.multiply(t2);
        var c1 = t3.multiply(this._x1);

        return new F2elm(c0, c1);
    }
    multiply(y) {
        var y0 = y._x0;
        var y1 = y._x1;

        var t1 = this._x0.multiply(y0);
        var t2 = this._x1.multiply(y1);
        var c0 = t1.subtract(t2);
        t1 = t1.add(t2);
        t2 = this._x0.add(this._x1);
        var c1 = y0.add(y1);
        c1 = t2.multiply(c1);
        c1 = c1.subtract(t1);

        return new F2elm(c0, c1);
    }
    isEven() {
        return this._x0.isEven() && this._x1.isEven();
    }
    div2() {
        var c0 = this._x0.div2();
        var c1 = this._x1.div2();

        return new F2elm(c0, c1);
    }
    inverse() {
        var t0 = this._x0.square();
        var t1 = this._x1.square();
        t0 = t0.add(t1);
        t0 = t0.inverse();
        t1 = this._x1.negate();

        var c0 = t0.multiply(this._x0);
        var c1 = t1.multiply(t0);

        return new F2elm(c0, c1);
    }

    inv4Way(z0, z1, z2, z3)
    {
        var res = [undefined, undefined, undefined, undefined];

        res[0] = z0.multiply(z1);
        res[1] = z2.multiply(z3);
        res[2] = res[0].multiply(res[1]);
        res[3] = res[2].inverse();
        res[2] = res[1].multiply(res[3]);
        res[3] = res[0].multiply(res[3]);
        res[0] = res[2].multiply(z1);
        res[1] = res[2].multiply(z0);
        res[2] = res[3].multiply(z3);
        res[3] = res[3].multiply(z2);

        return res;
    }

    swap(y, option) {
        var y0 = this._x0.swap(y._x0, option);
        var y1 = this._x1.swap(y._x1, option);

        return new F2elm(y0, y1);
    }

    select(y, option) {
        var mask = option.negate();
        var y0 = (y._x0).getValue();
        var y1 = (y._x1).getValue();

        var bix0 = this._x0.getValue();
        var bix1 = this._x1.getValue();

        var z0 = bix0.xor(y0);
        var z1 = bix1.xor(y1);

        z0 = z0.and(mask);
        z1 = z1.and(mask);
        z0 = bix0.xor(z0);
        z1 = bix1.xor(z1);

        return new F2elm(z0, z1);


    }

    toString() {
        return this._x1 + "*i + " + this._x0;
    }

    toByteArray() {
        var ret = new Uint8Array(this._x0._psize * 2);
        ret.set(this._x0.toByteArray());
        ret.set(this._x1.toByteArray(), this._x0._psize);
        return ret;
    }
    getPrime() {
        return this._x0.getPrime();
    }
}

F2elm.ZERO = new F2elm(Felm.ZERO, Felm.ZERO);
F2elm.ONE = new F2elm(Felm.ONE, Felm.ONE);

module.exports = F2elm;