const Felm = require('./Felm.js');
class PointProjective {
    static fromInts(xc, zc) {
        if(xc._p != zc._p)
        {
            throw new Error('Cannot make point projective with field elements from different prime fields');
        }
        return new PointProjective(new Felm(xc, xc._p), new Felm(zc, zc._p));
    }
    static fromPointProjective(p) {
        return new PointProjective(p._x, p._z);
    }
    constructor(xc, zc) {
        this._x = xc;
        this._z = zc;
    }
    getX() {
        return this._x;
    }
    getZ() {
        return this._z;
    }
    swapPointsBasefield(q, option) {
        var qx = this._x.swap(q._x, option);
        var qz = this._z.swap(q._z, option);

        return new PointProjective(qx, qz);
    }
    toString() {
        return '(' + this._x + ', ' + this._z + ')';
    }
}

module.exports = PointProjective;